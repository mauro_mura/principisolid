package eu.inginfo.SOLID.ISP;

public interface ServiceLocatorInterface {
    FactoryInterface get(String name);
}
