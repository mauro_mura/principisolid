package eu.inginfo.SOLID.ISP;

public interface MutableServiceContainerInterface {
    void set(String name, FactoryInterface factory);
}
