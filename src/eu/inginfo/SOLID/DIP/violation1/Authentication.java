package eu.inginfo.SOLID.DIP.violation1;

public class Authentication {
    private UserProviderInterface userProvider;

    public Authentication(UserProviderInterface userProvider) {
        this.userProvider = userProvider;
    }

    public void checkCredentials(String username, String password) {
        Object user = this.userProvider.findUser(username);

        // ...
    }
}
