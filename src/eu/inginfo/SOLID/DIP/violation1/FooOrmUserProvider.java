package eu.inginfo.SOLID.DIP.violation1;

public class FooOrmUserProvider implements UserProviderInterface {
    private Connection connection;

    public FooOrmUserProvider(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Object findUser(String username) {
        return this.connection.fetchAssoc(
                "SELECT * FROM users WHERE username = ?",
                new Object[]{ username },
                User.class);
    }
}
