package eu.inginfo.SOLID.DIP.violation1;

public interface UserProviderInterface {
    Object findUser(String username);
}
