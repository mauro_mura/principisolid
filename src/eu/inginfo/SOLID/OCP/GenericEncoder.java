package eu.inginfo.SOLID.OCP;

public class GenericEncoder {
    private EncoderFactoryInterface encoderFactory;

    public GenericEncoder(EncoderFactoryInterface encoderFactory) {
        this.encoderFactory = encoderFactory;
    }

    public String encodeToFormat(String data, String format) {
        EncoderInterface encoder = this.encoderFactory.createForFormat(format);
        return encoder.encode(data);
    }
}
