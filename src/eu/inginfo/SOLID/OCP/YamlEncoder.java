package eu.inginfo.SOLID.OCP;

public class YamlEncoder implements EncoderInterface {
    @Override
    public String encode(String data) {
        return "YAML";
    }
}
