package eu.inginfo.SOLID.OCP;

public class XmlEncoder implements EncoderInterface {
    @Override
    public String encode(String data) {
        return "XML";
    }
}
