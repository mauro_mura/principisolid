package eu.inginfo.SOLID.OCP;

public interface EncoderInterface {
    String encode(String data);
}
