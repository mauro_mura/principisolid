package eu.inginfo.SOLID.OCP;

public class Main {
    public static void main(String[] args) {
        EncoderFactory encoderFactory = new EncoderFactory();
        encoderFactory.addEncoderFactory("xml", new XmlEncoder());
        encoderFactory.addEncoderFactory("json", new JsonEncoder());
        encoderFactory.addEncoderFactory("yaml", new YamlEncoder());
        GenericEncoder genericEncoder = new GenericEncoder(encoderFactory);
        genericEncoder.encodeToFormat("TEST", "json");
    }
}
