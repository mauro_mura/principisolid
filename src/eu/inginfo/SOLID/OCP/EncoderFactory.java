package eu.inginfo.SOLID.OCP;

import java.util.HashMap;
import java.util.Map;

public class EncoderFactory implements EncoderFactoryInterface {
    private Map<String,EncoderInterface> factories = new HashMap<>();

    public void addEncoderFactory(String format, EncoderInterface encoder) {
        this.factories.put(format, encoder);
    }

    @Override
    public EncoderInterface createForFormat(String format) {
        return this.factories.get(format);
    }
}
