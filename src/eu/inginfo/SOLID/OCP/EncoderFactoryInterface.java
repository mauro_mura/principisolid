package eu.inginfo.SOLID.OCP;

public interface EncoderFactoryInterface {
    EncoderInterface createForFormat(String format);
}
