package eu.inginfo.SOLID.LSP.violation2;

public class SmtpMassMailer implements MassMailerInterface {
    @Override
    public void sendMail(TransportInterface transport, MessageInterface message, RecipientsInterface recipients) {
        if (!(transport instanceof SmtpTransport)) {
            throw new RuntimeException("SmtpMassMailer only works with SMTP");
        }
    }
}
