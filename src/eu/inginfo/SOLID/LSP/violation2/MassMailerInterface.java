package eu.inginfo.SOLID.LSP.violation2;

public interface MassMailerInterface {
    void sendMail(TransportInterface transport, MessageInterface message, RecipientsInterface recipients);
}
