package eu.inginfo.SOLID.LSP.violation1;

public interface FileWithOwnerInterface extends FileInterface {
    void changeOwner(String user, String group);
}
