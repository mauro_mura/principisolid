package eu.inginfo.SOLID.SRP;

import java.util.Map;

public interface TemplatingEngineInterface {
    String render(String templateFile, Map<String,String> arguments);
}
