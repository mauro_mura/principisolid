package eu.inginfo.SOLID.SRP;

public class ConfirmationMailMailer {
    private ConfirmationMailFactory confirmationMailFactory;
    private MailerInterface mailer;

    public ConfirmationMailMailer(
            ConfirmationMailFactory confirmationMailFactory,
            MailerInterface mailer) {
        this.confirmationMailFactory = confirmationMailFactory;
        this.mailer = mailer;
    }

    public void sendTo(User user) {
        MessageInterface message = this.createMessageFor(user);
        this.sendMessage(message);
    }

    private MessageInterface createMessageFor(User user) {
        return this.confirmationMailFactory.createMessageFor(user);
    }

    private void sendMessage(MessageInterface message) {
        this.mailer.send(message);
    }
}
