package eu.inginfo.SOLID.SRP;

public class Message implements MessageInterface {
    private String subject;
    private String body;
    private String to;

    public Message(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public String getSubject() {

        return subject;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
