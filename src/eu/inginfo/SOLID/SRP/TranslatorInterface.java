package eu.inginfo.SOLID.SRP;

public interface TranslatorInterface {
    String translate(String text);
}
