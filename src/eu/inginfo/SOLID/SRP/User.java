package eu.inginfo.SOLID.SRP;

public class User {
    private String confirmationCode = "123456";
    private String emailAddress = "email@example.com";

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
