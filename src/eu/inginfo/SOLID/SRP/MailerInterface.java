package eu.inginfo.SOLID.SRP;

public interface MailerInterface {
    void send(MessageInterface message);
}
