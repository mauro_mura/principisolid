package eu.inginfo.SOLID.SRP;

import java.util.HashMap;
import java.util.Map;

public class ConfirmationMailFactory {
    private TemplatingEngineInterface templating;
    private TranslatorInterface translator;

    public ConfirmationMailFactory(
            TemplatingEngineInterface templating,
            TranslatorInterface translator) {
        this.templating = templating;
        this.translator = translator;
    }

    public MessageInterface createMessageFor(User user) {
        String subject = this.translator.translate("Confirm your mail address");
        Map<String,String> arguments = new HashMap<>();
        arguments.put("confirmationCode", user.getConfirmationCode());
        String body = this.templating.render("confirmationMail.html.tpl", arguments);
        Message message = new Message(subject,body);
        message.setTo(user.getEmailAddress());
        return message;
    }
}
